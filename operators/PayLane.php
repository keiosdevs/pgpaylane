<?php namespace Keios\PGPayLane\Operators;

use Finite\StatefulInterface;
use Keios\PaymentGateway\Support\HashIdsHelper;
use Keios\PaymentGateway\Support\OperatorUrlizer;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Keios\PaymentGateway\ValueObjects\PaymentResponse;
use Keios\PaymentGateway\Traits\SettingsDependent;
use Keios\PGPayLane\Classes\PayLaneChargeMaker;
use Keios\PaymentGateway\Core\Operator;


/**
 * Class PayLane
 *
 * @package Keios\PGPayLane
 */
class PayLane extends Operator implements StatefulInterface
{
    use SettingsDependent;

    const CREDIT_CARD_REQUIRED = true;

    /**
     * @var string
     */
    public static $operatorCode = 'keios.pgpaylane::lang.operators.paylane';

    /**
     * @var string
     */
    public static $operatorLogoPath = '/plugins/keios/pgpaylane/assets/img/paylane/logo.png';

    /**
     * @var string
     */
    public static $modeOfOperation = 'api';

    /**
     * @var array
     */
    public static $configFields = [];

    /**
     * @return \Keios\PaymentGateway\ValueObjects\PaymentResponse
     */
    public function sendPurchaseRequest()
    {


        $chargeMaker = new PayLaneChargeMaker($this->cart, $this->paymentDetails, $this->creditCard);
        $charge = $chargeMaker->make();
        dd($charge);
        $this->setApiKey();



        try {
            $response = Charge::create($charge);

        } catch (\PayLane\Error\Card $ex) {

            $message = $this->makeCardErrorMessage($ex);

            return new PaymentResponse($this, null, [$message]);

        } catch (\Exception $e) {

            return new PaymentResponse($this, null, [$e->getMessage()]);
        }



        $this->isPaidInPayLane = $response['paid'];
        $this->chargeId = $response['id'];

        $internalRedirect = \URL::to(
            '_paymentgateway/' . OperatorUrlizer::urlize($this) . '?pgUuid=' . base64_encode($this->uuid)
        );

        return new PaymentResponse($this, $internalRedirect);
    }

    /**
     * @param array $data
     *
     * @return \Symfony\Component\HttpFoundation\RedirectResponse
     */
    public function processNotification(array $data)
    {
        if ($this->isPaidInPayLane && $this->can(Operator::TRANSITION_ACCEPT)) {
            try {
                $this->accept();
            } catch (\Exception $ex) {
                \Log::error($ex->getMessage());
            }

            return \Redirect::to($this->returnUrl);
        } else {
            return \Redirect::to($this->returnUrl);
        }
    }

    /**
     * @return \Keios\PaymentGateway\ValueObjects\PaymentResponse
     */
    public function sendRefundRequest()
    {
        $this->setApiKey();

        try {

            $charge = Charge::retrieve($this->chargeId);
            $charge->refunds->create();

        } catch (\Exception $e) {

            return new PaymentResponse($this, null, [$e->getMessage()]);
        }

        // if no exception occured, we assume refund was created

        return new PaymentResponse($this, null);
    }

    /**
     * @param array $data
     *
     * @return string
     */
    public static function extractUuid(array $data)
    {
        if (isset($data['pgUuid'])) {
            return base64_decode($data['pgUuid']);
        } else {
            throw new \RuntimeException('Invalid redirect, payment uuid is missing.');
        }
    }

    protected function setApiKey()
    {
        $this->getSettings();

        $paylaneApiKey = $this->getSettings()->get('paylane.apiKey');

        //PayLaneApi::setApiKey($paylaneApiKey);
        $request = new PayLane\Request($paylaneApiKey);
    }

    protected function makeCardErrorMessage(\PayLane\Error\Card $exception)
    {
        $body = $exception->getJsonBody();
        $error = $body['error'];

        $errArr = [];

        $errArr['status'] = $e->getHttpStatus();
        array_key_exists('type', $error) ? $errArr['type'] = $error['type'] : false;
        array_key_exists('code', $error) ? $errArr['code'] = $error['code'] : false;
        array_key_exists('param', $error) ? $errArr['parameter'] = $error['param'] : false;
        array_key_exists('message', $error) ? $errArr['message'] = $error['message'] : false;

        $message = 'Error from PayLane: ';

        foreach ($errArr as $errKey => $errValue) {
            $message .= $errKey . ': ' . $errValue . ', ';
        }

        return $message;
    }
}
