<?php

return [
    'labels' => [
        'pluginDesc' => 'PayLane Integration Extension for Keios PaymentGateway',
    ],
    'operators' => [
        'paylane' => 'PayLane',
    ],
    'settings' => [
        'tab' => 'PayLane',
        'general' => 'General settings',
        'apiKey' => 'You PayLane ApiKey',
        'testMode' => 'Use test mode',
    ],
    'info' => [
        'header' => 'How to retrieve your PayLane api key',
    ],
];