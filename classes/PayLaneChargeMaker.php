<?php namespace Keios\PGPayLane\Classes;

use Keios\PaymentGateway\Contracts\Orderable;
use Keios\PaymentGateway\ValueObjects\Details;
use Keios\PaymentGateway\ValueObjects\CreditCard;

/**
 * Class PayLaneChargeMaker
 *
 * @package Keios\PGPayLane\Classes
 */
class PayLaneChargeMaker
{
    /**
     * @var Orderable
     */
    protected $cart;

    /**
     * @var Details
     */
    protected $details;

    /**
     * @var CreditCard
     */
    protected $card;

    /**
     * PayLaneChargeMaker constructor.
     *
     * @param Orderable  $cart
     * @param Details    $details
     * @param CreditCard $card
     */
    public function __construct(Orderable $cart, Details $details, CreditCard $card)
    {

        $this->cart = $cart;
        $this->details = $details;
        $this->card = $card;
    }

    /**
     * @return array
     */
    public function make()
    {
        $cost = $this->cart->getTotalGrossCost(true);

        $parsedCard = $this->parseCard($this->card);
        dd($cost);
        $charge = [
            'sale'                 => [
                'amount'      => $cost->getAmountString(),
                'currency'    => strtolower($cost->getCurrency()->getIsoCode()),
                'description' => $this->details->getDescription(),
            ],
            'customer'             => [
                'name' => $this->card->getFirstName().' '.$this->card->getLastName(),
                'email' => $this->details->getEmail(),
            ],
            'card' => [
                'token' => $parsedCard
            ]

        ];

        if ($this->cart->hasShipping()) {
            $shipping = $this->cart->getShipping();
            $charge['shipping'] = [
                'name'     => $shipping->getName(),
                'cost'     => $shipping->getGrossCost()->getAmountBasic(),
                'currency' => $shipping->getGrossCost()->getCurrency()->getIsoCode(),
                'tax'      => $shipping->getTax().'%',
            ];
        }

        return $charge;

    }

    /**
     * @param CreditCard $card
     *
     * @return array
     */
    protected function parseCard(CreditCard $card)
    {
        return [
            'object'    => 'card',
            'number'    => $card->getNumber(),
            'exp_month' => (int)$card->getExpiryMonth(),
            'exp_year'  => $card->getExpiryYear(),
            'cvc'       => $card->getCvv(),
            'name'      => $card->getFirstName().' '.$card->getLastName(),
        ];
    }
}