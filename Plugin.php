<?php namespace Keios\PGPayLane;

use Keios\PaymentGateway\Models\Settings as PaymentGatewaySettings;
use System\Classes\PluginBase;
use Event;

/**
 * PG-PGPayLane Plugin Information File
 *
 * @package Keios\PGPayLane
 */
class Plugin extends PluginBase
{
    public $require = [
        'Keios.PaymentGateway'
    ];

    /**
     * Returns information about this plugin.
     *
     * @return array
     */
    public function pluginDetails()
    {
        return [
            'name'        => 'PG-PayLane',
            'description' => 'keios.pgpaylane::lang.labels.pluginDesc',
            'author'      => 'Keios',
            'icon'        => 'icon-paylane'
        ];
    }

    public function register()
    {
        Event::listen(
            'paymentgateway.booted',
            function () {
                /**
                 * @var \October\Rain\Config\Repository $config
                 */
                $config = $this->app['config'];
                $config->push('keios.paymentgateway.operators', 'Keios\PGPayLane\Operators\PayLane');
            }
        );

        Event::listen(
            'backend.form.extendFields',
            function ($form) {

                if (!$form->model instanceof PaymentGatewaySettings) {
                    return;
                }

                if ($form->context !== 'general') {
                    return;
                }

                /**
                 * @var \Backend\Widgets\Form $form
                 */
                $form->addTabFields(
                    [
                        'paylane.general'  => [
                            'label' => 'keios.pgpaylane::lang.settings.general',
                            'tab'   => 'keios.pgpaylane::lang.settings.tab',
                            'type'  => 'section',
                        ],
                        'paylane.info'     => [
                            'type' => 'partial',
                            'path' => '@/plugins/keios/pgpaylane/partials/_paylane_info.htm',
                            'tab'  => 'keios.pgpaylane::lang.settings.tab',
                        ],
                        'paylane.apiKey'   => [
                            'label'   => 'keios.pgpaylane::lang.settings.apiKey',
                            'tab'     => 'keios.pgpaylane::lang.settings.tab',
                            'type'    => 'text',
                            'default' => ''
                        ],
                        'paylane.testMode' => [
                            'label' => 'keios.pgpaylane::lang.settings.testMode',
                            'tab'   => 'keios.pgpaylane::lang.settings.tab',
                            'type'  => 'switch'
                        ],
                    ]
                );
            }
        );
    }
}
